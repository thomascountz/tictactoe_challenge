package main

import (
	"os"

	"bitbucket.org/thomascountz/tictactoe_challenge/tictactoe"
)

func main() {
	board := tictactoe.MakeBoard()
	rules := tictactoe.MakeRules()
	prompter := tictactoe.MakePrompter(os.Stdout)

	consoleStrategy := tictactoe.MakeConsoleStrategy(os.Stdin)
	minimaxStrategy := tictactoe.MakeMinimaxStrategy(&rules, &prompter, tictactoe.O)
	playerOne := tictactoe.MakePlayer(tictactoe.X, consoleStrategy)
	playerTwo := tictactoe.MakePlayer(tictactoe.O, minimaxStrategy)

	players := tictactoe.Players{&playerOne, &playerTwo}
	engine := tictactoe.MakeEngine(&prompter, &board, &rules, players)
	engine.Start()
}

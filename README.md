# TicTacToe

## Quick Start for MacOS

For non-MacOS users, please see the installation instructions below.

To play tic-tac-toe on MacOS, clone this repository:

```
$ git clone https://thomascountz@bitbucket.org/thomascountz/tictactoe_challenge.git
```

`cd` into `tictactoe_challenge/`:

```
$ cd tictactoe_challenge
```

And run the included executable:

```
$ ./tictactoe_challenge
```

Troubleshooting:
Ensure that you have permission to run `./tictactoe_challenge` as an executable. You many need to adjust your permissions:

```
$ chmod 755 ./tictactoe_challenge
```

## Installation

In order to build an executable for platforms other than MacOS, or to install this package for development and testing, you must first install the Go language by following the [official installation instructions](https://golang.org/doc/install).

After following the instructions, you can install this package in your workspace:

```
$ go get bitbucket.org/thomascountz/tictactoe_challenge
```

After this, you can `cd` into the project directory:

```
$ cd $GOPATH/src/bitbucket.org/thomascountz/tictactoe_challenge
```

Finally, you can install the dependencies, again, using `go get`:

```
$ go get -t ./...
```

## Running Tests

After following the about installation instructions, unit tests can be run with the following command, after navigating into the package directory:

```
$ go test ./...
```

## Buildling an Executable

You can build an executable for your platform using `go build`. Once in the project directory, run the following command:

```
$ go build
```

## CircleCI

This project is run through CircleCI.

[![CircleCI](https://circleci.com/bb/thomascountz/tictactoe_challenge/tree/master.svg?style=svg&circle-token=4306459eb0987c446c097a2bab8f08c776c2aefb)](https://circleci.com/bb/thomascountz/tictactoe_challenge/tree/master)

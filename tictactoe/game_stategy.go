package tictactoe

type GameStrategy interface {
	ReadMove(board Board) (int, error)
}

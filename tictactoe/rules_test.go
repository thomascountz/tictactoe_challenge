package tictactoe

import (
	"testing"

	"github.com/stretchrcom/testify/assert"
)

func TestTie(t *testing.T) {
	t.Run("it returns TRUE if the board is complete but IS NOT won", func(t *testing.T) {
		board := &board{playArea: [9]Symbol{X, X, O, O, O, X, X, O, X}}

		rules := MakeRules()

		result := rules.Tie(board)

		assert.True(t, result)
	})

	t.Run("it returns FALSE if the board is not complete", func(t *testing.T) {
		board := &board{playArea: [9]Symbol{E, E, E, E, E, E, E, E, E}}

		rules := MakeRules()

		result := rules.Tie(board)

		assert.False(t, result)
	})

	t.Run("it returns FALSE when the board contains winner", func(t *testing.T) {
		board := &board{playArea: [9]Symbol{X, X, X, O, X, O, O, X, O}}

		rules := MakeRules()

		result := rules.Tie(board)

		assert.False(t, result)
	})
}

func TestWin(t *testing.T) {
	t.Run("it returns TRUE when the board DOES contain winner along a row", func(t *testing.T) {
		board := &board{playArea: [9]Symbol{X, X, X, E, E, E, E, E, E}}

		rules := MakeRules()

		result := rules.Win(board)

		assert.True(t, result)
	})

	t.Run("it returns TRUE when the board DOES contain winner along a column", func(t *testing.T) {
		board := &board{playArea: [9]Symbol{E, X, E, E, X, E, E, X, E}}

		rules := MakeRules()

		result := rules.Win(board)

		assert.True(t, result)
	})

	t.Run("it returns TRUE when the board DOES contain winner along a diagonal", func(t *testing.T) {
		board := &board{playArea: [9]Symbol{X, E, E, E, X, E, E, E, X}}

		rules := MakeRules()

		result := rules.Win(board)

		assert.True(t, result)
	})

	t.Run("it returns FALSE when the board DOES NOT contain winner", func(t *testing.T) {
		board := &board{playArea: [9]Symbol{X, X, O, O, O, X, X, O, X}}

		rules := MakeRules()

		result := rules.Win(board)

		assert.False(t, result)
	})

	t.Run("it returns FALSE if the board is not complete", func(t *testing.T) {
		board := &board{playArea: [9]Symbol{E, E, E, E, E, E, E, E, E}}

		rules := MakeRules()

		result := rules.Win(board)

		assert.False(t, result)
	})
}

func TestNextPlayer(t *testing.T) {
	t.Run("it returns playerOne when there is an odd number of empty spaces", func(t *testing.T) {
		playerOne := new(fakePlayer)
		playerTwo := new(fakePlayer)
		players := Players{playerOne, playerTwo}
		board := &board{playArea: [9]Symbol{E, E, E, E, E, E, E, E, E}}

		rules := MakeRules()

		expected := playerOne
		actual := rules.NextPlayer(players, board)

		assert.True(t, expected == actual)
	})

	t.Run("it returns playerTwo when there are is an even number of empty spaces", func(t *testing.T) {
		playerOne := new(fakePlayer)
		playerTwo := new(fakePlayer)
		players := Players{playerOne, playerTwo}
		board := &board{playArea: [9]Symbol{X, E, E, E, E, E, E, E, E}}

		rules := MakeRules()

		expected := playerTwo
		actual := rules.NextPlayer(players, board)

		assert.True(t, expected == actual)
	})
}

package tictactoe

import (
	"fmt"
	"io"
	"strconv"
)

type Prompter interface {
	WriteWelcomeMessage()
	WriteBoard(Board)
	WritePrompt(Player)
	WriteWinningMessage(Player)
	WriteTieMessage()
	WriteError(error)
	WriteMove(int)
}

type prompter struct {
	out io.Writer
}

func MakePrompter(out io.Writer) prompter {
	return prompter{out}
}

func (c prompter) WriteWelcomeMessage() {
	fmt.Fprintln(c.out, "Welcome to Tic Tac Toe!")
}

func (c prompter) WriteBoard(board Board) {
	fmt.Fprintf(c.out, "\n\n%s", board)
}

func (c prompter) WritePrompt(player Player) {
	fmt.Fprintf(c.out, "Player %s, it's your turn!\n", player.GetSymbol())
}

func (c prompter) WriteWinningMessage(player Player) {
	fmt.Fprintf(c.out, "Congratulations, Player %s has won!\n", player.GetSymbol())
}

func (c prompter) WriteTieMessage() {
	fmt.Fprintln(c.out, "Congratulations to you both! It's a tie!")
}

func (c prompter) WriteError(err error) {
	fmt.Fprintln(c.out, err)
}

func (c prompter) WriteMove(move int) {
	fmt.Fprintln(c.out, strconv.Itoa(move))
}

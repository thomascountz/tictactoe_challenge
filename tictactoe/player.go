package tictactoe

type Player interface {
	GetSymbol() Symbol
	GetMove(Board) (int, error)
}

type player struct {
	symbol       Symbol
	gameStrategy GameStrategy
}

func MakePlayer(symbol Symbol, gameStrategy GameStrategy) player {
	return player{symbol, gameStrategy}
}

func (p player) GetSymbol() Symbol {
	return p.symbol
}

func (p player) GetMove(board Board) (int, error) {
	return p.gameStrategy.ReadMove(board)
}

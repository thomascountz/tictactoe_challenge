package tictactoe

import (
	"github.com/stretchr/testify/mock"
)

type fakeStrategy struct {
	mock.Mock
}

func (fs *fakeStrategy) ReadMove(board Board) (int, error) {
	args := fs.Called(board)
	return args.Int(0), args.Error(1)
}

type fakePrompter struct {
	mock.Mock
}

func (fp *fakePrompter) WriteWelcomeMessage() {
	fp.Called()
}

func (fp *fakePrompter) WriteBoard(board Board) {
	fp.Called(board)
}

func (fp *fakePrompter) WritePrompt(player Player) {
	fp.Called(player)
}

func (fp fakePrompter) WriteWinningMessage(player Player) {
	fp.Called(player)
}

func (fp fakePrompter) WriteTieMessage() {
	fp.Called()
}

func (fp fakePrompter) WriteError(err error) {
	fp.Called(err)
}

func (fp fakePrompter) WriteMove(move int) {
	fp.Called(move)
}

type fakePlayer struct {
	mock.Mock
}

func (fp *fakePlayer) GetSymbol() Symbol {
	args := fp.Called()
	return args.Get(0).(Symbol)
}

func (fp *fakePlayer) GetMove(board Board) (int, error) {
	args := fp.Called(board)
	return args.Int(0), args.Error(1)
}

type fakeBoard struct {
	mock.Mock
}

func (fb *fakeBoard) PlayArea() [9]Symbol {
	args := fb.Called()
	return args.Get(0).([9]Symbol)
}

func (fb *fakeBoard) PlaceSymbol(index int, symbol Symbol) Board {
	args := fb.Called(index, symbol)
	return args.Get(0).(Board)
}

func (fb *fakeBoard) SpacesAvailable() []int {
	args := fb.Called()
	return args.Get(0).([]int)
}

func (fb *fakeBoard) IsValid(move int) bool {
	args := fb.Called(move)
	return args.Bool(0)
}

func (fb *fakeBoard) IsAvailable(move int) bool {
	args := fb.Called(move)
	return args.Bool(0)
}

func (fb *fakeBoard) String() string {
	args := fb.Called()
	return args.String(0)
}

type fakeRules struct {
	mock.Mock
}

func (fr *fakeRules) Tie(board Board) bool {
	args := fr.Called(board)
	return args.Bool(0)
}

func (fr *fakeRules) Win(board Board) bool {
	args := fr.Called(board)
	return args.Bool(0)
}

func (fr *fakeRules) NextPlayer(players Players, board Board) Player {
	args := fr.Called(players, board)
	return args.Get(0).(Player)
}

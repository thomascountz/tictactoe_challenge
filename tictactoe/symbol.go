package tictactoe

type Symbol string

const E Symbol = ""
const X Symbol = "X"
const O Symbol = "O"

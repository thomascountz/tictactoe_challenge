package tictactoe

import (
	"testing"

	"github.com/stretchr/testify/mock"
	"github.com/stretchrcom/testify/assert"
)

func TestMinimaxStrategyReadMove(t *testing.T) {
	t.Run("it returns the position of the only move available", func(t *testing.T) {
		board := &board{playArea: [9]Symbol{X, X, O, O, O, X, X, O, E}}
		rules := MakeRules()
		prompter := new(fakePrompter)
		prompter.On("WriteMove", mock.Anything).Return().Once()
		maxSymbol := X
		minimaxStrategy := MakeMinimaxStrategy(rules, prompter, maxSymbol)

		expected := 8
		actual, _ := minimaxStrategy.ReadMove(board)

		assert.Equal(t, expected, actual)
	})

	t.Run("it returns the position of the winning move", func(t *testing.T) {
		board := &board{playArea: [9]Symbol{O, X, X, O, E, X, X, E, O}}
		rules := MakeRules()
		prompter := new(fakePrompter)
		prompter.On("WriteMove", mock.Anything).Return().Once()
		maxSymbol := X
		minimaxStrategy := MakeMinimaxStrategy(rules, prompter, maxSymbol)

		expected := 4
		actual, _ := minimaxStrategy.ReadMove(board)

		assert.Equal(t, expected, actual)

	})

	t.Run("it returns the move to block the opponent", func(t *testing.T) {
		board := &board{playArea: [9]Symbol{X, E, E, E, O, E, X, E, E}}
		rules := MakeRules()
		prompter := new(fakePrompter)
		prompter.On("WriteMove", mock.Anything).Return().Once()
		maxSymbol := O
		minimaxStrategy := MakeMinimaxStrategy(rules, prompter, maxSymbol)

		expected := 3
		actual, _ := minimaxStrategy.ReadMove(board)

		assert.Equal(t, expected, actual)

	})
}

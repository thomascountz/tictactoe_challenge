package tictactoe

import (
	"errors"
	"fmt"
	"io"
	"strconv"
)

type consoleStrategy struct {
	in io.Reader
}

func MakeConsoleStrategy(in io.Reader) consoleStrategy {
	return consoleStrategy{in}
}

func (c consoleStrategy) ReadMove(board Board) (int, error) {
	input := c.readInput()
	move, err := c.convertToMove(input)
	if err != nil {
		err = errors.New("The position is invalid")
	} else if !board.IsValid(move) {
		err = errors.New("The position is invalid")
	} else if !board.IsAvailable(move) {
		err = errors.New("The position is unavailable")
	}
	return move, err
}

func (c consoleStrategy) readInput() string {
	var input string
	fmt.Fscanln(c.in, &input)
	return input
}

func (c consoleStrategy) convertToMove(input string) (int, error) {
	return strconv.Atoi(input)
}

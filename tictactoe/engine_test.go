package tictactoe

import (
	"errors"
	"testing"
)

func TestEngineStart(t *testing.T) {
	t.Run("it plays a round until a WIN", func(t *testing.T) {
		prompter := new(fakePrompter)
		board := new(fakeBoard)
		newBoard := new(fakeBoard)
		rules := new(fakeRules)
		playerOne := new(fakePlayer)
		playerTwo := new(fakePlayer)
		players := Players{playerOne, playerTwo}
		move := 5
		symbol := X

		engine := MakeEngine(prompter, board, rules, players)

		prompter.On("WriteWelcomeMessage").Return().Once()

		// Game is not over
		rules.On("NextPlayer", players, board).Return(playerOne).Once()
		prompter.On("WriteBoard", board).Return().Once()
		playerOne.On("GetSymbol").Return(symbol).Once()
		prompter.On("WritePrompt", playerOne).Return().Once()
		playerOne.On("GetMove", board).Return(move, nil).Once()
		board.On("PlaceSymbol", move, symbol).Return(newBoard).Once()

		// There is a win
		rules.On("Tie", newBoard).Return(false).Once()
		rules.On("Win", newBoard).Return(true).Once()
		prompter.On("WriteBoard", newBoard).Return().Once()
		prompter.On("WriteWinningMessage", playerOne).Return()

		engine.Start()

		playerOne.AssertExpectations(t)
		board.AssertExpectations(t)
		rules.AssertExpectations(t)
	})

	t.Run("it plays a round until a TIE", func(t *testing.T) {
		prompter := new(fakePrompter)
		board := new(fakeBoard)
		newBoard := new(fakeBoard)
		rules := new(fakeRules)
		playerOne := new(fakePlayer)
		playerTwo := new(fakePlayer)
		players := Players{playerOne, playerTwo}
		move := 5
		symbol := X

		engine := MakeEngine(prompter, board, rules, players)

		prompter.On("WriteWelcomeMessage").Return().Once()

		// Game is not over
		rules.On("NextPlayer", players, board).Return(playerOne).Once()
		prompter.On("WriteBoard", board).Return().Once()
		playerOne.On("GetSymbol").Return(symbol).Once()
		prompter.On("WritePrompt", playerOne).Return().Once()
		playerOne.On("GetMove", board).Return(move, nil).Once()
		board.On("PlaceSymbol", move, symbol).Return(newBoard).Once()

		// There is a tie
		rules.On("Tie", newBoard).Return(true).Once()
		prompter.On("WriteTieMessage").Return()
		prompter.On("WriteBoard", newBoard).Return().Once()

		engine.Start()

		playerOne.AssertExpectations(t)
		board.AssertExpectations(t)
		rules.AssertExpectations(t)
	})

	t.Run("it retries to get a player move until it is valid", func(t *testing.T) {
		prompter := new(fakePrompter)
		board := new(fakeBoard)
		newBoard := new(fakeBoard)
		rules := new(fakeRules)
		playerOne := new(fakePlayer)
		playerTwo := new(fakePlayer)
		players := Players{playerOne, playerTwo}
		move := 5
		symbol := X
		invalidMoveError := errors.New("The position is invalid")

		engine := MakeEngine(prompter, board, rules, players)

		prompter.On("WriteWelcomeMessage").Return().Once()

		// Game is not over
		rules.On("NextPlayer", players, board).Return(playerOne).Once()
		prompter.On("WriteBoard", board).Return().Once()
		playerOne.On("GetSymbol").Return(symbol).Once()
		prompter.On("WritePrompt", playerOne).Return().Once()

		// playerOne.GetMove() returns an error
		playerOne.On("GetMove", board).Return(0, invalidMoveError).Once()
		prompter.On("WriteError", invalidMoveError).Return().Once()
		prompter.On("WritePrompt", playerOne).Return().Once()

		// playerOne.GetMove() returns a valid move
		playerOne.On("GetMove", board).Return(move, nil).Once()
		board.On("PlaceSymbol", move, symbol).Return(newBoard).Once()

		// There is a win
		rules.On("Tie", newBoard).Return(false).Once()
		rules.On("Win", newBoard).Return(true).Once()
		prompter.On("WriteBoard", newBoard).Return().Once()
		prompter.On("WriteWinningMessage", playerOne).Return()

		engine.Start()

		playerOne.AssertExpectations(t)
		board.AssertExpectations(t)
		rules.AssertExpectations(t)
	})
}

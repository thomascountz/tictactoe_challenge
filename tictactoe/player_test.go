package tictactoe

import (
	"errors"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestHumanPlayerGetSymbol(t *testing.T) {
	t.Run("returns a string of the player's symbol", func(t *testing.T) {
		strategy := new(fakeStrategy)
		player := MakePlayer(X, strategy)

		expected := X
		actual := player.GetSymbol()

		assert.Equal(t, expected, actual)
	})
}

func TestGetMove(t *testing.T) {
	t.Run("returns after delegating to a strategy", func(t *testing.T) {
		board := new(fakeBoard)

		strategy := new(fakeStrategy)
		move := 4
		err := errors.New("ERROR")
		strategy.On("ReadMove", board).Return(move, err).Once()

		player := MakePlayer(X, strategy)

		expectedMove, expectedError := move, err
		actualMove, actualError := player.GetMove(board)

		assert.Equal(t, expectedMove, actualMove)
		assert.Equal(t, expectedError, actualError)
	})
}

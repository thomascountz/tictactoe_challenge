package tictactoe

import (
	"bytes"
	"errors"
	"strconv"
	"testing"

	"github.com/stretchrcom/testify/assert"
)

func TestWriteWelcomeMessage(t *testing.T) {
	t.Run("Writes a welcome message", func(t *testing.T) {
		writer := bytes.Buffer{}
		prompter := MakePrompter(&writer)

		prompter.WriteWelcomeMessage()

		expected := "Welcome"
		actual := writer.String()

		assert.Contains(t, actual, expected)
	})
}

func TestWriteBoard(t *testing.T) {
	t.Run("Writes a Board", func(t *testing.T) {
		writer := bytes.Buffer{}
		prompter := MakePrompter(&writer)
		board := MakeBoard()

		prompter.WriteBoard(board)

		expected := board.String()
		actual := writer.String()

		assert.Contains(t, actual, expected)
	})
}

func TestWritePrompt(t *testing.T) {
	t.Run("Writes a prompt for a player", func(t *testing.T) {
		writer := bytes.Buffer{}
		prompter := MakePrompter(&writer)
		player := new(fakePlayer)
		symbol := X
		player.On("GetSymbol").Return(symbol).Once()

		prompter.WritePrompt(player)

		expected := symbol
		actual := writer.String()

		assert.Contains(t, actual, expected)
	})
}

func TestWriteWinningMessage(t *testing.T) {
	t.Run("Writes a congraulatory message to the winning player", func(t *testing.T) {
		writer := bytes.Buffer{}
		prompter := MakePrompter(&writer)
		player := new(fakePlayer)
		symbol := X
		player.On("GetSymbol").Return(symbol).Once()

		prompter.WriteWinningMessage(player)

		expected := symbol
		actual := writer.String()

		assert.Contains(t, actual, expected)
	})
}

func TestWriteTieMessage(t *testing.T) {
	t.Run("Writes a congraulatory message to both players", func(t *testing.T) {
		writer := bytes.Buffer{}
		prompter := MakePrompter(&writer)

		prompter.WriteTieMessage()

		expected := "tie"
		actual := writer.String()

		assert.Contains(t, actual, expected)
	})
}

func TestWriteError(t *testing.T) {
	t.Run("Writes an Error", func(t *testing.T) {
		writer := bytes.Buffer{}
		prompter := MakePrompter(&writer)
		err := errors.New("ERROR")

		prompter.WriteError(err)

		expected := err.Error()
		actual := writer.String()

		assert.Contains(t, actual, expected)
	})
}

func TestWriteMove(t *testing.T) {
	t.Run("Writes a move", func(t *testing.T) {
		writer := bytes.Buffer{}
		prompter := MakePrompter(&writer)
		move := 3

		prompter.WriteMove(move)

		expected := strconv.Itoa(3)
		actual := writer.String()

		assert.Contains(t, actual, expected)
	})
}

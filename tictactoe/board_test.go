package tictactoe

import (
	"testing"

	"github.com/stretchrcom/testify/assert"
)

func TestPlayArea(t *testing.T) {
	t.Run("it is an array of 9 Symbols", func(t *testing.T) {
		board := MakeBoard()

		expected := 9
		actual := len(board.PlayArea())

		assert.Equal(t, expected, actual)
	})

	t.Run("it is an array of E Symbols", func(t *testing.T) {
		board := MakeBoard()

		for _, cell := range board.PlayArea() {
			expected := E
			actual := cell

			assert.Equal(t, expected, actual)
		}
	})
}

func TestPlaceSymbol(t *testing.T) {
	t.Run("it updates the board with a given symbol at a given index", func(t *testing.T) {
		board := MakeBoard()

		index := 0
		symbol := X

		newBoard := board.PlaceSymbol(index, symbol)

		expected := [9]Symbol{X, E, E, E, E, E, E, E, E}
		actual := newBoard.PlayArea()

		assert.Equal(t, expected, actual)
	})
}

func TestSpacesAvailable(t *testing.T) {
	t.Run("returns a list of available spaces in the board's PlayArea for an EMPTY board", func(t *testing.T) {
		board := MakeBoard()

		expected := []int{0, 1, 2, 3, 4, 5, 6, 7, 8}
		actual := board.SpacesAvailable()

		assert.Equal(t, expected, actual)
	})

	t.Run("returns a list of available spaces in the board's PlayArea for an NON-EMPTY board", func(t *testing.T) {
		board := MakeBoard()
		newBoard := board.PlaceSymbol(0, X)

		expected := []int{1, 2, 3, 4, 5, 6, 7, 8}
		actual := newBoard.SpacesAvailable()

		assert.Equal(t, expected, actual)
	})
}
func TestIsValid(t *testing.T) {
	t.Run("returns true if move IS within the range of the board's playArea", func(t *testing.T) {
		board := MakeBoard()
		move := 0

		result := board.IsValid(move)

		assert.True(t, result)
	})

	t.Run("returns false if move IS NOT within the range of the board's playArea", func(t *testing.T) {
		board := MakeBoard()
		move := 9

		result := board.IsValid(move)

		assert.False(t, result)
	})

	t.Run("returns false if move IS NOT within the range of the board's playArea", func(t *testing.T) {
		board := MakeBoard()
		move := -1

		result := board.IsValid(move)

		assert.False(t, result)
	})
}

func TestIsAvailable(t *testing.T) {
	t.Run("returns true if cell at the given index IS empty", func(t *testing.T) {
		emptyBoard := MakeBoard()
		index := 0

		result := emptyBoard.IsAvailable(index)

		assert.True(t, result)
	})

	t.Run("returns false if cell at the given index IS NOT empty", func(t *testing.T) {
		board := board{playArea: [9]Symbol{X, E, E, E, E, E, E, E, E}}
		index := 0

		result := board.IsAvailable(index)

		assert.False(t, result)
	})
}

func TestToString(t *testing.T) {
	t.Run("returns a string representation of an EMPTY board", func(t *testing.T) {
		board := MakeBoard()

		actual := board.String()
		expected := "┌───┬───┬───┐\n" +
			"│ 0 │ 1 │ 2 │\n" +
			"│───│───│───│\n" +
			"│ 3 │ 4 │ 5 │\n" +
			"│───│───│───│\n" +
			"│ 6 │ 7 │ 8 │\n" +
			"└───┴───┴───┘\n"

		assert.Equal(t, expected, actual)
	})

	t.Run("returns a string representation of an NON-EMPTY board", func(t *testing.T) {
		board := MakeBoard()
		newBoard := board.PlaceSymbol(0, X)

		actual := newBoard.String()
		expected := "┌───┬───┬───┐\n" +
			"│ X │ 1 │ 2 │\n" +
			"│───│───│───│\n" +
			"│ 3 │ 4 │ 5 │\n" +
			"│───│───│───│\n" +
			"│ 6 │ 7 │ 8 │\n" +
			"└───┴───┴───┘\n"

		assert.Equal(t, expected, actual)
	})
}

package tictactoe

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestFirst(t *testing.T) {
	t.Run("it returs the first player", func(t *testing.T) {
		playerOne := new(fakePlayer)
		playerTwo := new(fakePlayer)

		players := Players{playerOne, playerTwo}

		expected := playerOne
		actual := players.First()

		assert.True(t, expected == actual)
	})
}

func TestSecond(t *testing.T) {
	t.Run("it returs the second player", func(t *testing.T) {
		playerOne := new(fakePlayer)
		playerTwo := new(fakePlayer)

		players := Players{playerOne, playerTwo}

		expected := playerTwo
		actual := players.Second()

		assert.True(t, expected == actual)
	})
}

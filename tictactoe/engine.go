package tictactoe

type Engine struct {
	prompter Prompter
	board    Board
	rules    Rules
	players  Players
}

func MakeEngine(prompter Prompter, board Board, rules Rules, players Players) Engine {
	return Engine{prompter, board, rules, players}
}

func (e *Engine) Start() {
	e.prompter.WriteWelcomeMessage()
	for {
		currentPlayer := e.rules.NextPlayer(e.players, e.board)
		e.prompter.WriteBoard(e.board)

		playerSymbol := currentPlayer.GetSymbol()
		e.prompter.WritePrompt(currentPlayer)

		move, err := currentPlayer.GetMove(e.board)

		for err != nil {
			e.prompter.WriteError(err)
			e.prompter.WritePrompt(currentPlayer)
			move, err = currentPlayer.GetMove(e.board)
		}

		e.board = e.board.PlaceSymbol(move, playerSymbol)

		if e.rules.Tie(e.board) {
			e.prompter.WriteTieMessage()
			break
		}

		if e.rules.Win(e.board) {
			e.prompter.WriteWinningMessage(currentPlayer)
			break
		}

	}
	e.prompter.WriteBoard(e.board)
}

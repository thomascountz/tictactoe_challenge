package tictactoe

import (
	"strconv"
)

type Board interface {
	PlayArea() [9]Symbol
	PlaceSymbol(int, Symbol) Board
	SpacesAvailable() []int
	IsValid(int) bool
	IsAvailable(int) bool
	String() string
}

type board struct {
	playArea [9]Symbol
}

func MakeBoard() board {
	return board{playArea: [9]Symbol{E, E, E, E, E, E, E, E, E}}
}

func (b board) PlayArea() [9]Symbol {
	return b.playArea
}

func (b board) PlaceSymbol(index int, symbol Symbol) Board {
	newPlayArea := b.playArea
	newPlayArea[index] = symbol
	return board{playArea: newPlayArea}
}

func (b board) IsValid(index int) bool {
	return index >= 0 && index < len(b.playArea)
}

func (b board) IsAvailable(index int) bool {
	return b.playArea[index] == E
}

func (b board) SpacesAvailable() []int {
	spacesAvailable := []int{}
	for space, cell := range b.PlayArea() {
		if cell == E {
			spacesAvailable = append(spacesAvailable, space)
		}
	}
	return spacesAvailable
}

func (b board) String() string {
	return "┌───┬───┬───┐\n" +
		"│ " + b.spaceToString(0) + " │ " + b.spaceToString(1) + " │ " + b.spaceToString(2) + " │\n" +
		"│───│───│───│\n" +
		"│ " + b.spaceToString(3) + " │ " + b.spaceToString(4) + " │ " + b.spaceToString(5) + " │\n" +
		"│───│───│───│\n" +
		"│ " + b.spaceToString(6) + " │ " + b.spaceToString(7) + " │ " + b.spaceToString(8) + " │\n" +
		"└───┴───┴───┘\n"
}

func (b board) spaceToString(index int) string {
	cell := b.PlayArea()[index]
	if cell == E {
		return strconv.Itoa(index)
	}
	return string(cell)
}

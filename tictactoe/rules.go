package tictactoe

type Rules interface {
	Tie(Board) bool
	Win(Board) bool
	NextPlayer(Players, Board) Player
}

type rules struct{}

func MakeRules() rules {
	return rules{}
}

func (r rules) Tie(board Board) bool {
	return noSpacesAvailable(board) && !r.Win(board)
}

func (r rules) Win(board Board) bool {
	playArea := board.PlayArea()
	for _, combo := range winningCombos() {
		if playArea[combo[0]] != E &&
			playArea[combo[0]] == playArea[combo[1]] &&
			playArea[combo[1]] == playArea[combo[2]] {
			return true
		}
	}
	return false
}

func (r rules) NextPlayer(players Players, board Board) Player {
	nextPlayer := players.First()
	if len(board.SpacesAvailable())%2 == 0 {
		nextPlayer = players.Second()
	}
	return nextPlayer
}

func winningCombos() [][]int {
	return [][]int{
		[]int{0, 1, 2}, []int{3, 4, 5}, []int{6, 7, 8},
		[]int{0, 3, 6}, []int{1, 4, 7}, []int{2, 5, 8},
		[]int{2, 4, 6}, []int{0, 4, 8},
	}
}

func noSpacesAvailable(board Board) bool {
	return len(board.SpacesAvailable()) == 0
}

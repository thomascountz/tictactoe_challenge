package tictactoe

type minimaxStrategy struct {
	rules     Rules
	prompter  Prompter
	maxSymbol Symbol
	minSymbol Symbol
}

func MakeMinimaxStrategy(rules Rules, prompter Prompter, maxSymbol Symbol) minimaxStrategy {
	minSymbol := O
	if maxSymbol == O {
		minSymbol = X
	}
	return minimaxStrategy{rules, prompter, maxSymbol, minSymbol}
}

func (m minimaxStrategy) ReadMove(board Board) (int, error) {
	move, _ := m.max(board)
	m.prompter.WriteMove(move)
	return move, nil
}

func (m minimaxStrategy) min(board Board) (int, int) {
	bestScore := 1
	var currentScore int
	var bestMove int

	for _, move := range board.SpacesAvailable() {
		newBoard := board.PlaceSymbol(move, m.minSymbol)
		if m.rules.Tie(newBoard) {
			currentScore = 0
		} else if m.rules.Win(newBoard) {
			currentScore = -1
		} else {
			_, currentScore = m.max(newBoard)
		}

		if currentScore < bestScore {
			bestScore = currentScore
			bestMove = move
		}
	}
	return bestMove, bestScore
}

func (m minimaxStrategy) max(board Board) (int, int) {
	bestScore := -1
	var currentScore int
	var bestMove int

	for _, move := range board.SpacesAvailable() {
		newBoard := board.PlaceSymbol(move, m.maxSymbol)
		if m.rules.Tie(newBoard) {
			currentScore = 0
		} else if m.rules.Win(newBoard) {
			currentScore = 1
		} else {
			_, currentScore = m.min(newBoard)
		}

		if currentScore > bestScore {
			bestScore = currentScore
			bestMove = move
		}
	}
	return bestMove, bestScore
}

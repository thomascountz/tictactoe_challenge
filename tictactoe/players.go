package tictactoe

type Players [2]Player

func (p *Players) First() Player {
	return p[0]
}

func (p *Players) Second() Player {
	return p[1]
}

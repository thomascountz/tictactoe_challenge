package tictactoe

import (
	"bytes"
	"testing"

	"github.com/stretchrcom/testify/assert"
)

func TestReadMove(t *testing.T) {
	t.Run("returns a move as an int", func(t *testing.T) {
		board := new(board)
		reader := bytes.Buffer{}
		reader.WriteString("5")

		consoleStrategy := MakeConsoleStrategy(&reader)

		expected := 5
		actual, err := consoleStrategy.ReadMove(board)

		assert.Equal(t, expected, actual)
		assert.Nil(t, err)
	})

	t.Run("returns an error if the move cannot be converted to an int", func(t *testing.T) {
		board := new(board)
		reader := bytes.Buffer{}
		reader.WriteString("")

		consoleStrategy := MakeConsoleStrategy(&reader)

		_, err := consoleStrategy.ReadMove(board)

		assert.NotNil(t, err)
	})

	t.Run("returns an error if the move does not fit on the board", func(t *testing.T) {
		board := new(board)
		reader := bytes.Buffer{}
		reader.WriteString("1337")

		consoleStrategy := MakeConsoleStrategy(&reader)

		_, err := consoleStrategy.ReadMove(board)

		assert.NotNil(t, err)
	})

	t.Run("returns an error if the move is unavailable on the board", func(t *testing.T) {
		board := &board{playArea: [9]Symbol{X, E, E, E, E, E, E, E, E}}
		reader := bytes.Buffer{}
		reader.WriteString("0")

		consoleStrategy := MakeConsoleStrategy(&reader)

		_, err := consoleStrategy.ReadMove(board)

		assert.NotNil(t, err)
	})
}
